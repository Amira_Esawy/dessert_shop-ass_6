package dessertShop;

public class sundae extends IceCream{
	private String toppingName;
	private int costTopping;

	public sundae(String icecreamName, int costIcecream, String toppingname, int costTopping) {
	super(icecreamName,costIcecream);
	this.costTopping = costTopping;
	this.toppingName=toppingname;
	}
	public String gettoppingName(){
		return  toppingName;
	}
	public int getCost(){
		return super.getCost() + costTopping;
	}

}
