package dessertShop;

public class Cookie extends DessertItem{
	private int Cookiesnum;
	private int pricePerDozen;


	public Cookie(String name, int Cookiesnum, int pricePerDozen) {
		super(name);
		this.Cookiesnum =Cookiesnum;
		this.pricePerDozen = pricePerDozen;
	}

	
	public int getCookiesNum(){
		return Cookiesnum;
	}

	public int getPricePerDozen(){
		return pricePerDozen;
	}
	public int getCost(){
		int cost = (int)Math.round(Cookiesnum * pricePerDozen / 12);
		return cost;
	}


	
}
