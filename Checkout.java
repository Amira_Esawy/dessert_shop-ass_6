package dessertShop; 

public class Checkout{
	public static int numOfItems = 0;
	private DessertItem[] arrofItems;

	public Checkout(){
		this.arrofItems = new DessertItem[100];
	}

	public void enterItem(DessertItem newItem){
		arrofItems[numOfItems] = newItem;
		numOfItems++;
	}

	

	public void clear(){
		arrofItems = new DessertItem[100];
		numOfItems = 0;
	}

	public int numberOfItems(){
		return numOfItems;
	}

	public int totalCost(){ // without tax
		int cost = 0;
		for (int i = 0; i < numOfItems; i++){
			cost += arrofItems[i].getCost();
		}
		return cost;
	}

	public int totalTax(){
		return (int)Math.round(totalCost() * DessertShoppe.TAX_RATE /100); 
	}

	public java.lang.String toString(){
		String s = "";
		//Formating: set column widths
		String leftColumn = "%-" + DessertShoppe.MAX_ITEM_NAME_SIZE + "s";
		String rightColumn = "%" + DessertShoppe.COST_WIDTH + "s";
		int recieptWidth = DessertShoppe.MAX_ITEM_NAME_SIZE + DessertShoppe.COST_WIDTH;

		//Name of store underlined by row of dashes
		String storeName = DessertShoppe.STORE_NAME;
		String dashes = "";
		//create string of dashes equal to length of store name
		for(int i = 0; i < storeName.length(); i++){
			dashes += "-";
		}
		//to center, I've added empty strings with a set width to act as padding
		String padding = "%" + (recieptWidth - storeName.length())/2 + "s";
		s += String.format(padding + "%s\n" + padding +"%s\n\n", "", storeName,"", dashes);
		
		//Item names and prices
		for (int i = 0; i < numOfItems; i++){
			String itemName = arrofItems[i].getName();
			String itemCost = DessertShoppe.cents2dollarsAndCents(arrofItems[i].getCost());
			if (arrofItems[i] instanceof Candy){
				double candyWeight = ((Candy)arrofItems[i]).getWeight();
				String candyCost = DessertShoppe.cents2dollarsAndCents(((Candy)arrofItems[i]).getPricePerPound());
				s += String.format("%,.2f lbs. @ %s /lb.\n", candyWeight, candyCost);
				//%,.2f is a double with 2 decimal places of precision
			}
			if (arrofItems[i] instanceof Cookie){
				int numCookies = ((Cookie)arrofItems[i]).getCookiesNum();
				String cookieCost = DessertShoppe.cents2dollarsAndCents(((Cookie)arrofItems[i]).getPricePerDozen());
				s += String.format("%d @ %s /dz.\n", numCookies, cookieCost);
			}
			if (arrofItems[i] instanceof sundae){
				String toppingName = ((sundae)arrofItems[i]).gettoppingName();
				s += String.format("%s Sundae with\n", toppingName);
			}
			s += String.format(leftColumn + rightColumn + "\n", itemName, itemCost); 
		}

		//Tax
		String tax = DessertShoppe.cents2dollarsAndCents(totalTax());
		s += String.format(leftColumn + rightColumn + "\n", "Tax", tax);

		//Total
		String total = DessertShoppe.cents2dollarsAndCents(totalTax() + totalCost());
		s += String.format(leftColumn + rightColumn + "\n", "Total Cost", total);
		
		return s;
	}
}
